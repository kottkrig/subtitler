const path = require("path");

const merge = require("webpack-merge");
const common = require("./webpack.common.js");

module.exports = merge(common, {
  devServer: {
    inline: true,
    stats: { colors: true },
    host: "0.0.0.0",
    disableHostCheck: true,
    overlay: true
  }
});
