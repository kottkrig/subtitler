port module Ports exposing
    ( InfoForElm(..)
    , InfoForOutside(..)
    , onInfoForElm
    , sendInfoOutside
    )

import Json.Decode exposing (decodeValue)
import Json.Encode exposing (Value)
import Model exposing (..)


type alias PortPayload =
    { tag : String
    , data : Value
    }


port infoForOutside : PortPayload -> Cmd msg


port infoForElm : (PortPayload -> msg) -> Sub msg


type InfoForOutside
    = SetVideoPlaybackRate Float
    | RequestTextFileUrl String


type InfoForElm
    = NewVideoFile File
    | NewTextFileUrl String


sendInfoOutside : InfoForOutside -> Cmd msg
sendInfoOutside info =
    case info of
        SetVideoPlaybackRate newPlaybackRate ->
            infoForOutside
                { tag = "SetVideoPlaybackRate"
                , data = Json.Encode.float newPlaybackRate
                }

        RequestTextFileUrl textFileData ->
            infoForOutside
                { tag = "RequestTextFileUrl"
                , data = Json.Encode.string textFileData
                }


onInfoForElm : (InfoForElm -> msg) -> (String -> msg) -> Sub msg
onInfoForElm tagger onError =
    infoForElm
        (\payload ->
            case payload.tag of
                "NewVideoFile" ->
                    case
                        decodeValue
                            (Json.Decode.map2 File
                                (Json.Decode.field "src" Json.Decode.string)
                                (Json.Decode.field "name" Json.Decode.string)
                            )
                            payload.data
                    of
                        Ok file ->
                            tagger (NewVideoFile file)

                        Err e ->
                            onError (Json.Decode.errorToString e)

                "NewTextFileUrl" ->
                    case
                        decodeValue Json.Decode.string payload.data
                    of
                        Ok textFileUrl ->
                            tagger (NewTextFileUrl textFileUrl)

                        Err e ->
                            onError (Json.Decode.errorToString e)

                _ ->
                    onError ("Payload came into Elm with unsupported tag: " ++ payload.tag)
        )
