"use strict";

// Require index.html so it gets copied to dist
require("./index.html");

const { Elm } = require("./Main.elm");

const mountNode = document.getElementById("main");
console.log("Elm", Elm);
// .embed() can take an optional second argument. This would be an object describing the data we need to start a program, i.e. a userID or some token
const app = Elm.Main.init({ node: mountNode, flags: null });

let textFileURL;

app.ports.infoForOutside.subscribe(({ tag, data }) => {
  console.log("infoForOutside:", tag, data);
  switch (tag) {
    case "RequestTextFileUrl":
      const fileUrl = createFileWithContent(data);
      app.ports.infoForElm({ tag: "NewTextFileUrl", data: fileUrl });
      break;
    case "SetVideoPlaybackRate":
      document.getElementById("video").playbackRate = data;
      break;

    default:
      break;
  }
});

const createFileWithContent = content => {
  const data = new Blob([content], { type: "text/plain" });

  if (textFileURL) {
    window.URL.revokeObjectURL(textFileURL);
  }

  textFileURL = window.URL.createObjectURL(data);
  return textFileURL;
};

function dropHandler(ev) {
  console.log("File(s) dropped");

  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();

  if (ev.dataTransfer.items) {
    // Use DataTransferItemList interface to access the file(s)
    for (var i = 0; i < ev.dataTransfer.items.length; i++) {
      // If dropped items aren't files, reject them
      if (ev.dataTransfer.items[i].kind === "file") {
        var file = ev.dataTransfer.items[i].getAsFile();
        let reader = new FileReader();
        reader.addEventListener(
          "load",
          e => {
            const blobUrl = e.target.result;
            console.log("... file[" + i + "] blobUrl = ", blobUrl);
            console.log("... file[" + i + "] file = ", file);
            app.ports.infoForElm({
              tag: "NewVideoFile",
              data: { src: blobUrl, name: file.name }
            });
          },
          false
        );
        reader.readAsDataURL(file);
      }
    }
  }

  // Pass event to removeDragData for cleanup
  removeDragData(ev);
}

function dragOverHandler(ev) {
  // console.log("File(s) in drop zone");

  // Prevent default behavior (Prevent file from being opened)
  ev.preventDefault();
}

function removeDragData(ev) {
  console.log("Removing drag data");

  if (ev.dataTransfer.items) {
    // Use DataTransferItemList interface to remove the drag data
    ev.dataTransfer.items.clear();
  }
}

document.body.addEventListener("drop", dropHandler);
document.body.addEventListener("dragover", dragOverHandler);
