module Model exposing (File, PlaybackRate(..), Subtitle, Video)





type alias Subtitle =
    { text : String
    , startTime : Float
    , selected : Bool
    , duration : Maybe Float
    }


type alias Video =
    { file : File
    , currentTime : Float
    , duration : Float
    , playbackRate : PlaybackRate
    }


type alias File =
    { src : String
    , name : String
    }


type PlaybackRate
    = Slow
    | Regular
    | Fast
