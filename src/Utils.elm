module Utils exposing (addSubtitlesToText, convertPlaybackRateToFloat, encodeVTT, encodeVTTHelper, encodeVTTItem, formattedTime, reject, removeSubtitleFromText, replaceFileExtension, takeHeadSubtitle)

import Model exposing (..)
import Regex exposing (..)


replaceFileExtension : String -> String -> String
replaceFileExtension videoFileName newFileExtension =
    Regex.replace fileExtension (\_ -> newFileExtension) videoFileName


fileExtension = 
    Maybe.withDefault Regex.never <|
        Regex.fromString "(\\.\\w*$)"


formattedTime : Float -> String
formattedTime time =
    let
        hours =
            floor (time / 3600)

        minutes =
            floor (time / 60)

        seconds =
            modBy 60 (floor time)

        milliseconds =
            round ((time - toFloat (floor time)) * 1000)
    in
    ""
        ++ String.padLeft 2 '0' (String.fromInt hours)
        ++ ":"
        ++ String.padLeft 2 '0' (String.fromInt minutes)
        ++ ":"
        ++ String.padLeft 2 '0' (String.fromInt seconds)
        ++ "."
        ++ String.padLeft 3 '0' (String.fromInt milliseconds)


encodeVTT : List Subtitle -> Float -> String
encodeVTT subtitles duration =
    "WEBVTT\n\n"
        ++ encodeVTTHelper subtitles duration


encodeVTTHelper : List Subtitle -> Float -> String
encodeVTTHelper subtitles videoDuration =
    case subtitles of
        [] ->
            ""

        subtitle :: restSubtitles ->
            case subtitle.duration of
                Just duration ->
                    encodeVTTItem subtitle.startTime (subtitle.startTime + duration) subtitle.text
                        ++ encodeVTTHelper restSubtitles videoDuration

                Nothing ->
                    case List.head restSubtitles of
                        Just nextSubtitle ->
                            encodeVTTItem subtitle.startTime nextSubtitle.startTime subtitle.text
                                ++ encodeVTTHelper restSubtitles videoDuration

                        Nothing ->
                            encodeVTTItem subtitle.startTime videoDuration subtitle.text


encodeVTTItem : Float -> Float -> String -> String
encodeVTTItem startTime endTime text =
    ""
        ++ formattedTime startTime
        ++ " --> "
        ++ formattedTime endTime
        ++ "\n"
        ++ text
        ++ "\n\n"


convertPlaybackRateToFloat : PlaybackRate -> Float
convertPlaybackRateToFloat playbackRate =
    case playbackRate of
        Regular ->
            1.0

        Slow ->
            0.5

        Fast ->
            2.0


takeHeadSubtitle : String -> Maybe String
takeHeadSubtitle text =
    case text of
        "" ->
            Nothing

        _ ->
            List.head (Regex.split newLines text)


newLines = 
    Maybe.withDefault Regex.never <|
        Regex.fromString "\n{2,}"

removeSubtitleFromText : String -> String -> String
removeSubtitleFromText rawText subtitleText =
    rawText
    |> String.split subtitleText
    |> String.join ""
    |> String.trim


addSubtitlesToText : List Subtitle -> String -> String
addSubtitlesToText subtitles text =
    let
        subtitlesAsText =
            subtitles
                |> List.map (\s -> s.text)
                |> List.intersperse "\n\n"
                |> String.concat
    in
    String.trim (subtitlesAsText ++ "\n\n" ++ text)


reject : (a -> Bool) -> List a -> List a
reject f l =
    List.filter (\a -> not (f a)) l
