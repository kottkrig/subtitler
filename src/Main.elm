module Main exposing (Msg(..), downloadButton, init, main, onDurationChange, onTimeUpdate, selectAllSubtitlesButton, subscriptions, targetCurrentTime, targetDuration, update, view, viewSubtitleItem, viewSubtitles)

import Browser
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Events exposing (..)
import Element.Font as Font
import Element.Input as Input
import Html exposing (Html)
import Html.Attributes exposing (autoplay, controls, src)
import Html.Events exposing (on)
import Json.Decode as Json exposing (Decoder, at, float, map, succeed)
import Model exposing (..)
import Palette
import Ports exposing (..)
import Utils


main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }



-- MODEL


init : Maybe String -> ( Model, Cmd msg )
init _ =
    ( Loaded
        (LoadedModel
            "(Birds chirping)\n\n\n(Music is playing)\n\n(Bird is shrieeeeeeking!!!)"
            []
            (Video (File "./temp.mp4" "temp.mp4") 0.0 0.0 Regular)
            Nothing
        )
    , Cmd.none
    )


type Model
    = WaitingForInput
    | Loaded LoadedModel


type alias LoadedModel =
    { rawText : String
    , subtitles : List Subtitle
    , video : Video
    , textFileUrl : Maybe String
    }



-- UPDATE


type Msg
    = Change String
    | AddSubtitle
    | RemoveSubtitles (List Subtitle)
    | TimeUpdate Float
    | DurationUpdate Float
    | SubtitleSelected Int Bool
    | SetPlaybackRate PlaybackRate
    | SelectAllSubtitles
    | DeselectAllSubtitles
    | ChangeSubtitleDuration Int Float
    | InfoForElm Ports.InfoForElm
    | LogError String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Loaded loadedModel ->
            updateLoadedModel msg loadedModel

        WaitingForInput ->
            updateWaitingForInputModel msg model


updateWaitingForInputModel msg model =
    case msg of
        InfoForElm infoForElm ->
            case infoForElm of
                Ports.NewVideoFile newFile ->
                    let
                        newModel =
                            LoadedModel
                                ""
                                []
                                (Video newFile 0.0 0.0 Regular)
                                Nothing
                    in
                    ( Loaded newModel, Cmd.none )

                _ ->
                    ( WaitingForInput, Cmd.none )

        _ ->
            ( WaitingForInput, Cmd.none )


updateLoadedModel : Msg -> LoadedModel -> ( Model, Cmd Msg )
updateLoadedModel msg model =
    case msg of
        InfoForElm infoForElm ->
            case infoForElm of
                Ports.NewVideoFile newFile ->
                    ( Loaded { model | video = Video newFile 0.0 0.0 Regular }, Cmd.none )

                Ports.NewTextFileUrl newTextFileUrl ->
                    ( Loaded { model | textFileUrl = Just newTextFileUrl }, Cmd.none )

        Change newContent ->
            ( Loaded { model | rawText = newContent }, Cmd.none )

        AddSubtitle ->
            let
                newSubtitleText =
                    Utils.takeHeadSubtitle model.rawText

                newSubtitles =
                    case newSubtitleText of
                        Just text ->
                            model.subtitles ++ [ Subtitle text model.video.currentTime False Nothing ]

                        Nothing ->
                            model.subtitles

                newRawText =
                    case newSubtitleText of
                        Just text ->
                            Utils.removeSubtitleFromText model.rawText text

                        Nothing ->
                            model.rawText

                nextCmd =
                    Ports.sendInfoOutside (Ports.RequestTextFileUrl (Utils.encodeVTT newSubtitles model.video.duration))
            in
            ( Loaded
                { model
                    | subtitles = newSubtitles
                    , rawText = newRawText
                }
            , nextCmd
            )

        TimeUpdate newTime ->
            let
                oldVideo =
                    model.video

                newVideo =
                    { oldVideo | currentTime = newTime }
            in
            ( Loaded { model | video = newVideo }, Cmd.none )

        DurationUpdate newDuration ->
            let
                oldVideo =
                    model.video

                newVideo =
                    { oldVideo | duration = newDuration }
            in
            ( Loaded { model | video = newVideo }, Cmd.none )

        ChangeSubtitleDuration index newDuration ->
            let
                subtitle =
                    List.head (List.drop index model.subtitles)

                newSubtitles =
                    case subtitle of
                        Just s ->
                            List.take index model.subtitles ++ { s | duration = Just newDuration } :: List.drop (index + 1) model.subtitles

                        Nothing ->
                            model.subtitles

                nextCmd =
                    Ports.sendInfoOutside (Ports.RequestTextFileUrl (Utils.encodeVTT newSubtitles model.video.duration))
            in
            ( Loaded { model | subtitles = newSubtitles }, nextCmd )

        SubtitleSelected index checked ->
            let
                subtitle =
                    List.head (List.drop index model.subtitles)

                newSubtitles =
                    case subtitle of
                        Just s ->
                            List.take index model.subtitles ++ { s | selected = checked } :: List.drop (index + 1) model.subtitles

                        Nothing ->
                            model.subtitles
            in
            ( Loaded { model | subtitles = newSubtitles }, Cmd.none )

        SetPlaybackRate newPlaybackRate ->
            let
                oldVideo =
                    model.video

                newVideo =
                    { oldVideo | playbackRate = newPlaybackRate }

                newCmd =
                    Ports.sendInfoOutside (Ports.SetVideoPlaybackRate (Utils.convertPlaybackRateToFloat newPlaybackRate))
            in
            ( Loaded { model | video = newVideo }, newCmd )

        RemoveSubtitles subtitlesToBeRemoved ->
            let
                subtitles =
                    Utils.reject (\s -> List.member s subtitlesToBeRemoved) model.subtitles

                newRawText =
                    Utils.addSubtitlesToText subtitlesToBeRemoved model.rawText
            in
            ( Loaded
                { model
                    | subtitles = subtitles
                    , rawText = newRawText
                }
            , Cmd.none
            )

        SelectAllSubtitles ->
            let
                newSubtitles =
                    List.map (\s -> { s | selected = True }) model.subtitles
            in
            ( Loaded { model | subtitles = newSubtitles }, Cmd.none )

        DeselectAllSubtitles ->
            let
                newSubtitles =
                    List.map (\s -> { s | selected = False }) model.subtitles
            in
            ( Loaded { model | subtitles = newSubtitles }, Cmd.none )

        LogError string ->
            let
                _ =
                    Debug.log "LogError" string
            in
            ( Loaded model, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Ports.onInfoForElm InfoForElm LogError
        ]



-- Custom event handler


onTimeUpdate : (Float -> msg) -> Html.Attribute msg
onTimeUpdate msg =
    Html.Events.on "timeupdate" (Json.map msg targetCurrentTime)


onDurationChange : (Float -> msg) -> Html.Attribute msg
onDurationChange msg =
    Html.Events.on "durationchange" (Json.map msg targetDuration)


targetCurrentTime : Json.Decoder Float
targetCurrentTime =
    Json.at [ "target", "currentTime" ] Json.float


targetDuration : Json.Decoder Float
targetDuration =
    Json.at [ "target", "duration" ] Json.float



-- VIEW


view : Model -> Html Msg
view model =
    case model of
        WaitingForInput ->
            viewWaitingForInput

        Loaded loadedModel ->
            viewLoadedModel loadedModel


viewWaitingForInput =
    Element.layout []
        (el
            [ Background.color Palette.white
            , Font.color Palette.darkGray
            , Font.size Palette.normal
            , Font.family
                Palette.fonts
            , padding Palette.small
            ]
            (text "You need to drop a video file here")
        )


viewLoadedModel : LoadedModel -> Html Msg
viewLoadedModel model =
    let
        allSubtitles =
            model.subtitles

        selectedSubtitles =
            List.filter (\s -> s.selected) model.subtitles

        isSubtitlesSelected =
            not (List.isEmpty selectedSubtitles)

        video =
            model.video
    in
    Element.layout
        [ Background.color Palette.white
        , Font.color Palette.darkGray
        , Font.size Palette.normal
        , Font.family
            Palette.fonts
        , padding Palette.small
        ]
        (column
            [ height fill, width fill, spacing Palette.normal ]
            [ row [ spacing Palette.normal ]
                [ el
                    [ height (px 200) ]
                    (html
                        (Html.video
                            [ src video.file.src
                            , autoplay True
                            , controls True
                            , Html.Attributes.attribute "id" "video"
                            , Html.Attributes.attribute "muted" ""
                            , Html.Attributes.attribute "height" "100%"
                            , onTimeUpdate TimeUpdate
                            , onDurationChange DurationUpdate
                            ]
                            []
                        )
                    )
                , column
                    [ spacing Palette.normal ]
                    [ Input.radioRow
                        [ spacing Palette.normal ]
                        { onChange = SetPlaybackRate
                        , selected = Just video.playbackRate
                        , label = Input.labelAbove [] (text "Playback speed:")
                        , options =
                            [ Input.option Slow (text "Slow")
                            , Input.option Regular (text "Regular")
                            , Input.option Fast (text "Fast")
                            ]
                        }
                    , text (Utils.formattedTime video.currentTime ++ " / " ++ Utils.formattedTime video.duration)
                    ]
                ]
            , row
                [ height fill, width fill, spacing Palette.normal ]
                [ Input.multiline
                    [ padding Palette.normal, width fill, height fill ]
                    { label = Input.labelAbove [] (el [] (text "Your script:"))
                    , placeholder = Nothing
                    , text = model.rawText
                    , spellcheck = True
                    , onChange = Change
                    }
                , column
                    [ width fill, height fill ]
                    [ row
                        [ width fill, spacing Palette.xSmall ]
                        [ selectAllSubtitlesButton allSubtitles selectedSubtitles
                        , smallButton "Remove selected subtitles" (Just (RemoveSubtitles selectedSubtitles))
                        ]
                    , el [ width fill, height fill ] (viewSubtitles video.currentTime allSubtitles)
                    ]
                ]
            , row
                [ height (px 50), spacing Palette.xSmall ]
                [ smallButton "Add subtitle" (Just AddSubtitle)
                , el [ width fill, height fill ] (downloadButton video.file.name model.textFileUrl)
                ]
            ]
        )


smallButtonAttributes =
    [ padding Palette.small
    , Font.size Palette.small
    , Background.color Palette.darkGray
    , Font.color Palette.white
    , Font.variant Font.smallCaps
    , Element.mouseOver
        [ Background.color Palette.red
        ]
    ]


smallButton : String -> Maybe Msg -> Element Msg
smallButton textLabel maybeMsg =
    Input.button
        smallButtonAttributes
        { onPress = maybeMsg, label = text textLabel }


viewSubtitles currentTime subtitles =
    column
        [ height fill ]
        (List.indexedMap
            (\index subtitle ->
                el
                    []
                    (Input.checkbox
                        [ padding 10 ]
                        { onChange = SubtitleSelected index
                        , icon = Input.defaultCheckbox
                        , checked = subtitle.selected
                        , label = Input.labelRight [] (viewSubtitleItem currentTime subtitle index)
                        }
                    )
            )
            subtitles
        )


viewSubtitleItem currentTime subtitle index =
    let
        isEven =
            modBy 2 index == 0

        backgroundColor =
            if isEven then
                Palette.lightGray

            else
                Palette.white

        rowAttributes =
            [ spacing Palette.normal
            , Background.color backgroundColor
            , padding Palette.small
            ]
    in
    case subtitle.duration of
        Just duration ->
            row
                rowAttributes
                [ column
                    []
                    [ el [] (text (Utils.formattedTime subtitle.startTime ++ " -> " ++ Utils.formattedTime (subtitle.startTime + duration)))
                    , el [] (text subtitle.text)
                    ]
                ]

        Nothing ->
            row
                rowAttributes
                [ column
                    []
                    [ el [] (text (Utils.formattedTime subtitle.startTime))
                    , el [] (text subtitle.text)
                    ]
                , smallButton "Add duration" (Just (ChangeSubtitleDuration index (currentTime - subtitle.startTime)))
                ]


downloadButton : String -> Maybe String -> Element msg
downloadButton fileName textFileUrl =
    let
        element =
            el smallButtonAttributes (text "Save subtitles")

        textFileName =
            Utils.replaceFileExtension fileName ".vtt"
    in
    case textFileUrl of
        Just t ->
            downloadAs [] { label = element, filename = textFileName, url = t }

        Nothing ->
            element


buttonAttributes =
    [ Background.color (rgb255 200 200 200)
    , Font.color (rgb255 0 0 0)
    , padding 10
    ]


selectAllSubtitlesButton : List Subtitle -> List Subtitle -> Element Msg
selectAllSubtitlesButton allSubtitles selectedSubtitles =
    if List.isEmpty selectedSubtitles then
        smallButton "Select all" (Just SelectAllSubtitles)

    else
        smallButton "Deselect all" (Just DeselectAllSubtitles)
