module Palette exposing
    ( blue
    , darkGray
    , fonts
    , large
    , lightGray
    , normal
    , red
    , small
    , white
    , xSmall
    , xxSmall
    )

import Element
import Element.Font as Font


xxSmall =
    4


xSmall =
    8


small =
    16


normal =
    32


large =
    64


white =
    Element.rgb 1 1 1


lightGray =
    Element.rgb 0.9 0.9 0.9


darkGray =
    Element.rgb 0.1 0.1 0.1


red =
    Element.rgb 1 0 0


blue =
    Element.rgb 0 0 1


fonts =
    [ Font.external
        { url = "https://fonts.googleapis.com/css?family=EB+Garamond"
        , name = "EB Garamond"
        }
    , Font.sansSerif
    ]
